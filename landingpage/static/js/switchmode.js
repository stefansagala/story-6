$("input").click(() => {
    if ($("body").hasClass("light")) {
        $("body").removeClass("light");
        $("body").addClass("dark");
        $("#greetings").css("color","#99FFFF");
        $("#accordion").css("color","#99FFFF");
        $(".centered-box").css("background-color", "#1E90FF");
        $("p").css("border-color", "#87CEEB");
    }
    else {
        $("body").removeClass('dark');
        $("body").addClass('light');
        $("#greetings").css("color","#BDB76B");
        $("#accordion").css("color","#BDB76B");
        $(".centered-box").css("background-color", "#CD853F");
        $("p").css("border-color", "#F4A460");
    }
});
