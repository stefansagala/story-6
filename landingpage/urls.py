from django.urls import path
from . import views

APP_NAME = "landingpage"

urlpatterns = [
    path('', views.login, name='login'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('logout/', views.logout, name='logout'),
    path('status/', views.landing_page, name='landing_page'),
    path('library/', views.story8, name='story8'),
    path('biography/', views.story7, name='story7'),
]
