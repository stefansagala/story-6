from django.shortcuts import render, redirect
from .models import Statusku
from .forms import StatuskuForm
from django.http import HttpResponse
from django.contrib.auth import authenticate
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import login as auth_login
import datetime

# Create your views here.
def landing_page (request):
    formstatus = StatuskuForm(request.POST)
    if request.method == 'POST' :
        
        if formstatus.is_valid():
            formstatus.save()
            return redirect('landing_page')
    listformstatus = Statusku.objects.all()

    context = {
            'formstatus' : formstatus,
            'listformstatus' : listformstatus,
    }

    return render(request, 'hasilstatus.html', context)

def story7(request):
    return render(request, "story7.html")

def story8(request):
    return render(request, "story8.html")

def login(request): 
    if request.user.is_authenticated:
        return redirect('dashboard')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect ('dashboard')
            else:
                return render(request, 'login.html', {
                    'error' : 'Invalid username or password. Please enter the right username or password.'
                    }
                )
    return render(request, 'login.html')

def dashboard(request):
    return render(request, 'dashboard.html')
    

def logout (request):
    request.session.flush() 
    auth_logout(request)
    return redirect ('login')




