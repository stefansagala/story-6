from django.db import models
from datetime import datetime

# Create your models here.

class Statusku(models.Model):
    status = models.CharField(max_length=300)
    datetime = models.DateTimeField(default=datetime.now)
