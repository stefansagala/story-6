from django import forms
from .models import Statusku

class StatuskuForm(forms.ModelForm):
    status = forms.CharField(widget= forms.TextInput(attrs={
        "class" : "form-control",
        "required" : True,
        "placeholder" : "How are you?",
    }), label = '', empty_value='Please enter your status', max_length=300)

    class Meta:
        model = Statusku
        fields = {
            'status'
        } 

