from django.contrib.auth.models import User
from django.test import TestCase, Client
from importlib import import_module
from django.urls import resolve, reverse
from django.http import request
from .views import *
from .forms import *
from .models import *
from selenium import webdriver
import unittest
import time
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

# Create your tests here.
class Story6TestCase(TestCase):
    def test_landingpage_is_exist(self):
      response = Client().get('/status/')
      self.assertEqual(response.status_code, 200)

    def test_landingpage_404_error(self):
      response = Client().get('/page/')
      self.assertEqual(response.status_code, 404)
    
    def test_using_landingpage(self):
        found = resolve('/status/')
        self.assertEqual(found.func, landing_page)
    
    def test_uses_landingpage_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'hasilstatus.html')

    def test_model_landingpage(self):
        Statusku.objects.create(status = "Baik-baik Saja!")
        hitungJumlah = Statusku.objects.all().count()
        self.assertEqual(hitungJumlah,1)
    
    def test_inputhasilForm(self):
        response = self.client.post('/status/', {
            'status': "Baik-baik Saja!",
        })

        hitungJumlah = Statusku.objects.all().count()
        self.assertEqual(hitungJumlah,1)

    def test_uses_story7_template(self):
        response = Client().get('/biography/')
        self.assertTemplateUsed(response, 'story7.html')
    
    def test_uses_story7_title(self):
        response = Client().get('/biography/')
        html_response = response.content.decode('utf8')
        self.assertIn("My Story", html_response)
    
    def test_uses_story7_image(self):
        response = Client().get('/biography/')
        html_response = response.content.decode('utf8')
        self.assertIn('img', html_response)
    
    def test_uses_story7_accordion(self):
        response = Client().get('/biography/')
        html_response = response.content.decode('utf8')
        self.assertIn('accordion', html_response)

class Story6FunctionalTestCase (unittest.TestCase):
    def setUp (self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story6FunctionalTestCase, self).setUp()
    
    def tearDown(self):
        self.browser.quit()

    def testLandingPage (self):
        self.browser.get('http://localhost:8000/status/')
        self.assertIn('Your Status', self.browser.title)

        judul = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('How Are You?', judul)

        status = self.browser.find_element_by_id('id_status') 
        status.send_keys('CAPEK GAAAN')

        submit = self.browser.find_element_by_id('submit')      
        submit.send_keys(Keys.RETURN)

class LibraryTestCase(TestCase):
    def test_library_url_exists(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)

    def test_library_using_func(self):
        found = resolve('/library/')
        self.assertEqual(found.func, story8)

    def test_library_using_template(self):
        response = Client().get('/library/')
        self.assertTemplateUsed(response, 'story8.html')

class LoginTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('stefansagala', 'stefan@gmail.com', 'stefan123')
        cls.user.first_name = 'stefan'
        cls.user.last_name = 'sagala'
        cls.user.save()
    
    def test_Authentication_url_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_Authentication_using_Authentication_func(self):
        found = resolve('/')
        self.assertEqual(found.func, login)

    
    def test_Authentication_using_about_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'login.html')
        self.assertContains(response, 'username')
        self.assertContains(response, 'password')
    
    def test_SignIn_in_is_redirect(self):
        self.client.login(username='stefansagala', password='stefan123')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)

    def test_SignIn_not_SignedIn(self):
        response = self.client.get('/')
        html = response.content.decode()
        self.assertIn('<form', html)
    
    def test_SignIn_submit(self):
        response = self.client.post('/', {
                'username': 'stefansagala',
                'password' : 'stefan123',
            })
        
        self.assertEqual(response.status_code, 302)
        response2 = self.client.get('/dashboard/')
        html = response2.content.decode()
        self.assertIn(self.user.first_name, html)
    
    def test_SignIn_not_submit(self):
        response = self.client.post('/', {
                'username': 'stefan',
                'password' : 'stefan12345678',
            })
        
        self.assertEqual(response.status_code, 200)
    
    def test_SignIn_SignOut(self):
        response = self.client.post('/', {
                'username': 'stefansagala',
                'password' : 'stefan123',
            })

        self.client.get('/logout/')
        response = self.client.get('/')
        html = response.content.decode()
        self.assertNotIn(self.user.username, html)

    


    


    







